all: buildpdf

buildpdf:
	latexmk -pdf

clean:
	latexmk -c

cleanall:
	latexmk -C
