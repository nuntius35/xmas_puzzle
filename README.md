# Weihnachtsstern

Ein kleines Puzzle: Zerlegung eines fünfzackigen Sterns in 11 Teile.

![Zerlegung eines fünfzackigen Sterns in 11 Teile](star.png)

---

[Homepage of Andreas Geyer-Schulz](https://nuntius35.gitlab.io)
